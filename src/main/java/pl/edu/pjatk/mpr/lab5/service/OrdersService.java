package pl.edu.pjatk.mpr.lab5.service;

import java.util.ArrayList;
import java.util.LinkedList;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrdersService {

    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
        List<Order> orderList = orders.stream().filter(order -> order.getItems().size() > 5)
         .collect(Collectors.toList());
        return orderList;
    }

    public static List<Order> findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
        Map<Integer , List<Order>> sortOrderToAgeClient =  orders.stream()
                .collect(Collectors.groupingBy(order -> order.getClientDetails().getAge()));
        return sortOrderToAgeClient.get(sortOrderToAgeClient.size());
    }

    /*public static Order findOrderWithLongestComments(List<Order> orders) {
       
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {

    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {

    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {

    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {

    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        
    }
*/

}
