/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pjatk.mpr.lab5.service;

import java.util.Arrays;
import java.util.List;
import pl.edu.pjatk.mpr.lab5.model.Address;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

/**
 *
 * @author Patryk
 */
public class OrdersServiceTest {
  private static List<Order> orderList;
  private static List<Order> result;
  private static OrdersService serviceAPI; 
  private static ClientDetails client1 =  new ClientDetails();
  private static ClientDetails client2 =  new ClientDetails();
    public static void initOrder(){
 
    client1.setId(0);
    client1.setAge(30);
    client1.setLogin("Buzka123");
    client1.setName("Jozef");
    client1.setSurname("Burkiewicz");
    
    client2.setId(1);
    client2.setAge(50);
    client2.setLogin("Ziemniak");
    client2.setName("Aleksy");
    client2.setSurname("Niewiara");
    
    Address adress1 = new Address();
    adress1.setId(0);
    adress1.setStreet("Mokra");
    adress1.setBuildingNumber("10");
    adress1.setFlatNumber("12");
    adress1.setCity("Pazdziochowo");
    adress1.setPostalCode("82-010");
    adress1.setCountry("Polska");
    
     Address adress2 = new Address();
    adress2.setId(1);
    adress2.setStreet("Sucha");
    adress2.setBuildingNumber("13");
    adress2.setFlatNumber("18");
    adress2.setCity("Rybnik");
    adress2.setPostalCode("86-010");
    adress2.setCountry("Polska");
    
    OrderItem item1 = new OrderItem();
    item1.setId(0);
    item1.setName("Zapalki");
    item1.setPrice(0.30);
    
    OrderItem item2 = new OrderItem();
    item2.setId(1);
    item2.setName("Pluszak");
    item2.setPrice(6.30);
    
    OrderItem item3 = new OrderItem();
    item3.setId(3);
    item3.setName("Chusteczki");
    item3.setPrice(1.30);
    
    OrderItem item4 = new OrderItem();
    item4.setId(4);
    item4.setName("Chusteczki");
    item4.setPrice(1.30);
    
    OrderItem item5 = new OrderItem();
    item5.setId(5);
    item5.setName("Chusteczki");
    item5.setPrice(1.30);
    
    OrderItem item6 = new OrderItem();
    item6.setId(5);
    item6.setName("Chusteczki");
    item6.setPrice(1.30);
    
    List<OrderItem> items1;
    items1 = Arrays.asList(item1,item2,item3,item4,item5,item6);
    
    List<OrderItem> items2;
    items2 = Arrays.asList(item1,item2);
    
    Order order1 = new Order();
    order1.setId(0);
    order1.setClientDetails(client1);
    order1.setAddress(adress1);
    order1.setItems(items1);
    String comments1 = "Bardzo fajne zadanie, produkty by�y super. Uwielbiam pisa� testy. ELO";
    order1.setComments(comments1);
    Order order2 = new Order();
    order2.setId(1);
    order2.setClientDetails(client2);
    order2.setAddress(adress2);
    order2.setItems(items2);
    String comments2 = " Same testy zaj�y mi godzine. Spoko. Lubi� to";
    order2.setComments(comments2);
    orderList = Arrays.asList(order1,order2);
    }
    public static void main(String[] args)  {
    initOrder();
    System.out.println("---TEST----");
    System.out.println("FindOrderMore 5");
    System.out.println("Stock" + getOrdersThenMore5());

    }
    public static int getOrdersThenMore5 (){
     result = serviceAPI.findOrdersWhichHaveMoreThan5OrderItems(orderList);
     return result.size();
    }
    
}
